### Auteurs : Joël Ternoy / Nicolas Coeugniet


## SUJET ABORDE :  
Lignes de commandes et création d'images

 
 
## OBJECTIFS : 
Manipuler les dossiers et fichiers ainsi que l'écriture dans un fichier

## PRE-REQUIS : 
Déplacement dans l'arborescence par lignes de commande

## PREPARATION : 
Accès à un Terminal

## ELEMENTS DE COURS : 
* création de dossiers, de fichiers. 
* Ecriture dans un fichier
* Copie multiple par substituion de caractères
* Aperçu des fichiers pbm
* Protection

## SEANCE PRATIQUE :

### Création de dossiers

Lancer un terminal et créer un dossier nommé `TP1` dans votre répertoire courant à l'aide de la commande `mkdir`.

Se placer dans ce dossier et créer deux dossiers nommés `Images` et `Textes`

*Remarque: On peut créer plusieurs dossiers en une seule commande* 

### Création d'un fichier

Créer le fichier `essai.txt` dans le dossier `TP1` à l'aide de la commande `touch`. 

Taper `ls` pour vérifier que ce fichier est bien créé.

Déplacer le fichier `essai.txt` dans le dossier `Textes` à l'aide de la commande `mv`. 

Taper `cd Textes` de telle sorte à avoir le dossier `Textes` en répertoire courant.

Vérifier que le déplacement précédent à bien eu lieu.

### Ecriture dans un fichier

Un
fichier
au
format
PBM
est
un
fichier
en
ASCII
qui
se
compose
comme
suit
:

•
le
caractère
P1,
suivis
d’un
retour
à
la
ligne
ou
d’un
espace,

•
la
largeur
de
l’image
en
pixel,
en
base
10,
la
hauteur
de
l’image
en
pixel,
en
base
10,
suivies
d’un
retour
à
la
ligne
ou
d’un
espace,

•
la
liste
des
pixels,
ligne
par
ligne,
de
haut
en
bas
et
de
gauche
à
droite

Le but est par exemple d'écrire les lignes suivantes dans le fichier `essai.txt`:

```
P1 
3 3
0 0 0
1 1 1
1 0 1
```

Pour cela nous allons utiliser les redirections: 

notation  | effet
--------- | -----
`n>fichier` | redirige en **écriture** le descripteur *n* sur *fichier*
`n>>fichier` | redirige en **écriture** le descripteur *n* sur *fichier* en **concaténant** à la fin de *fichier*

Taper la commande `echo "P1">essai.txt` et vérifier ce qu'elle a effectué en lisant le fichier `essai.txt` à l'aide de la commande `cat`.

Que se passe-t-il si on tape ensuite `echo "3 3">essai.txt`?

Quelle correction apporter à la commande précédente pour que la ligne 

```
3 3
```
apparaisse à la ligne 2?

Vider le fichier `essai.txt` à l'aide de la commande `>essai.txt` puis reprendre l'écriture de ce fichier.

Vérifier que le fichier est bien celui demandé.
 
### Copie des fichiers

Pour copier un fichier sous un autre nom et extension, on utilise la  commande `cp`.

Copier le fichier `essai.txt` sous `essai.pbm` et visualiser le contenu de votre dossier.

Ouvrir votre fichier `essai.pbm` pour le visualiser.

### Une Nouvelle image
Créer un fichier `essai2.txt` pour qu'après copie sous `essai2.pbm`, ce dernier soit l'image de taille 5x5 suivante:

<img src="./essai.jpg" width="30"/>

### Un peu de rangement

Déplacer en une seule commande toutes les images dans le dossier Images en sachant que le symbole * remplace une suite quelconque de caractères.

### Protection

Lire attentivement cette page sur la [gestion des droits](https://fr.wikipedia.org/wiki/Permissions_UNIX)

Protéger vos fichiers .txt en écriture à l'aide de la fonction `chmod`.

Tester cette protection.

## Questions E3C : 
* Quelle arborescence est obtenue par ces lignes de commande?

```
mkdir NSI
cd NSI
touch TP1.py 
cd .. 
mkdir Corbeille
cp NSI/TP1.py Corbeille/
cd NSI
rm TP1.py
```

1. 

NSI - TP1.py

Corbeille - TP1.py

2.

NSI

Corbeille - TP1.py

3.

NSI - Corbeille - TP1.py

4.

NSI - TP1.py

Corbeille




* Que réalise la commande `>fichier.txt` ?

1. Elle permet d'effacer le contenu de fichier.txt 
2. Elle permet d'augmenter la taille de fichier.txt
3. Elle renvoie une erreur
4. Elle permet de créer le fichier fichier.txt
